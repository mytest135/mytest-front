import {createRouter, createWebHistory} from "vue-router/dist/vue-router"
import HomePage from "@/pages/HomePage";


const routes = [
    {
        path: "/",
        component: HomePage,

    },
    {
        path: "/product-add",
        component: () => import("../pages/ProducAddPage.vue"),

    },

]



export default createRouter({
    history: createWebHistory(),
    routes

})
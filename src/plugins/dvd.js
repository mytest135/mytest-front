import axios from "./axios"


export default {
    actions: {
        fetchDvds(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("/dvds")
                    .then((response) => {
                        console.log("Dvds ollindi hammasi joyida")
                        console.log(response)

                        let dvds = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }

                        context.commit("updateDvds", dvds)
                        resolve(dvds)
                    })
                    .catch(() => {
                        console.log("Dvds ollinmadi hatolik yuz berdi")
                        reject()
                    })
            })
        },

        pushDvd(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post("/dvds", data)
                    .then((response) => {
                        console.log("Hammasi joyida dvd qoshildi")
                        console.log(response)

                        let dvd = {
                            "@id": response.data['@id'],
                            "@type": response.data['@type'],
                            id: response.data.id,
                            sku: response.data.sku,
                            name: response.data.name,
                            price: response.data.price,
                            size: response.data.size,
                            isDeleted: response.data.isDeleted
                        }

                        context.commit('updateDvd', dvd)
                        resolve()
                    })
                    .catch((err) => {
                        console.log("Hattolik yuz dvd qoshishta")
                        console.log(err.response.status)
                        reject()
                    })
            })
        },


        changeDvd(context, dvdId) {
            return new Promise((resolve, reject) => {
                axios
                    .put("/dvds/" + dvdId, {isDeleted: true})
                    .then((response) => {
                        console.log("Hammasi joyida dvds ozgardi qoshildi")
                        console.log(response)

                        let dvd = {
                            "@id": response.data['@id'],
                            "@type": response.data['@type'],
                            id: response.data.id,
                            sku: response.data.sku,
                            name: response.data.name,
                            price: response.data.price,
                            size: response.data.size,
                            isDeleted: response.data.isDeleted
                        }

                        context.commit('updateDvd', dvd)
                        resolve()
                    })
                    .catch((err) => {
                        console.log("Hattolik yuz dvd ozgartirishda qoshishta")
                        console.log(err.response.status)
                        reject()
                    })
            })
        },

    },
    mutations: {
        updateDvd(state, dvd) {
            state.dvd = dvd
        },

        updateDvds(state, dvds) {
            state.dvds = dvds
        }

    },
    state: {

        dvd: {
            "@id": null,
            id: null,
            sku: null,
            name: null,
            price: null,
            size: null,
            isDeleted: null
        },
        dvds: {
            models: [],
            totalItems: 0
        }

    },
    getters: {


        getDvd(state) {
            return state.dvd
        },

        getDvds(state) {
            return state.dvds.models.filter((item) => {
                return item.isDeleted === false;
            })
        },

        getDvdIds(state) {
            let result = [];
            let obj = state.dvds.models.filter((item) => {
                return item.isDeleted === false
            });
            obj.forEach((item) => {
                result.push(item.sku);
            })
            return result;
        }




    }

}
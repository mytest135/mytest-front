import axios from "./axios"


export default {
    actions: {
        fetchFurnitures(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("/furniture")
                    .then((response) => {
                        console.log("furniture ollindi hammasi joyida")
                        console.log(response)

                        let furnitures = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }

                        context.commit("updateFurnitures", furnitures)
                        resolve(furnitures)
                    })
                    .catch(() => {
                        console.log("furnitures ollinmadi hatolik yuz berdi")
                        reject()
                    })
            })
        },

        pushFurniture(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post("/furniture", data)
                    .then((response) => {
                        console.log("Hammasi joyida furnitures qoshildi")
                        console.log(response)

                        let furniture = {
                            "@id": response.data['@id'],
                            "@type": response.data['@type'],
                            id: response.data.id,
                            sku: response.data.sku,
                            name: response.data.name,
                            price: response.data.price,
                            height: response.data.height,
                            width: response.data.width,
                            length: response.data.length,
                            isDeleted: response.data.isDeleted
                        }

                        context.commit('updateFurniture', furniture)
                        resolve()
                    })
                    .catch((err) => {
                        console.log("Hattolik yuz furniture qoshishta")
                        console.log(err.response.status)
                        reject()
                    })
            })
        },

        changeFurniture(context, furnitrueId) {
            return new Promise((resolve, reject) => {
                axios
                    .put("/furniture/" + furnitrueId, {isDeleted: true})
                    .then((response) => {
                        console.log("Hammasi joyida furnitures ozgardi qoshildi")
                        console.log(response)

                        let furniture = {
                            "@id": response.data['@id'],
                            "@type": response.data['@type'],
                            id: response.data.id,
                            sku: response.data.sku,
                            name: response.data.name,
                            price: response.data.price,
                            height: response.data.height,
                            width: response.data.width,
                            length: response.data.length,
                            isDeleted: response.data.isDeleted
                        }

                        context.commit('updateFurniture', furniture)
                        resolve()
                    })
                    .catch((err) => {
                        console.log("Hattolik yuz furniture ozgartirishda qoshishta")
                        console.log(err.response.status)
                        reject()
                    })
            })
        },

    },
    mutations: {
        updateFurniture(state, furniture) {
            state.furniture = furniture
        },

        updateFurnitures(state, furnitures) {
            state.furnitures = furnitures
        }

    },
    state: {

        furniture: {
            "@id": null,
            id: null,
            sku: null,
            name: null,
            price: null,
            height: null,
            width: null,
            length: null,
            isDeleted: null
        },
        furnitures: {
            models: [],
            totalItems: 0
        }

    },
    getters: {


        getFurniture(state) {
            return state.furniture
        },

        getFurnitures(state) {
            return state.furnitures.models.filter((item) => {
                return item.isDeleted === false;
            })
        },
        getFurnitureIds(state) {
            let result = [];
            let obj = state.furnitures.models.filter((item) => {
                return item.isDeleted === false
            });
            obj.forEach((item) => {
                result.push(item.sku);
            })
            return result;
        }




    }

}
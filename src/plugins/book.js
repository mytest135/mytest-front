import axios from "./axios"


export default {
    actions: {
        fetchBooks(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("/books")
                    .then((response) => {
                        console.log("Books ollindi hammasi joyida")
                        console.log(response)

                        let books = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }

                        context.commit("updateBooks", books)
                        resolve(books)
                    })
                    .catch(() => {
                        console.log("Books ollinmadi hatolik yuz berdi")
                        reject()
                    })
                    .finally(() => {
                        console.log("Finally ishlay veradi Books ollishda")
                    })
            })
        },

        pushBook(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post("/books", data)
                    .then((response) => {
                        console.log("Hammasi joyida book qoshildi")
                        console.log(response)

                        let book = {
                            "@id": response.data['@id'],
                            "@type": response.data['@type'],
                            id: response.data.id,
                            sku: response.data.sku,
                            name: response.data.name,
                            price: response.data.price,
                            weight: response.data.weight,
                            isDeleted: response.data.isDeleted
                        }

                        context.commit('updateBook', book)
                        resolve()
                    })
                    .catch((err) => {
                        console.log("Hattolik yuz book ollishda")
                        console.log(err.response.status)
                        reject()
                    })
            })
        },

        changeBook(context, bookId) {
            return new Promise((resolve, reject) => {
                axios
                    .put("/books/" + bookId, {isDeleted: true})
                    .then((response) => {
                        console.log("Hammasi joyida books ozgardi qoshildi")
                        console.log(response)

                        let book = {
                            "@id": response.data['@id'],
                            "@type": response.data['@type'],
                            id: response.data.id,
                            sku: response.data.sku,
                            name: response.data.name,
                            price: response.data.price,
                            weight: response.data.weight,
                            isDeleted: response.data.isDeleted
                        }

                        context.commit('updateBook', book)
                        resolve()
                    })
                    .catch((err) => {
                        console.log("Hattolik yuz book ozgartirishda qoshishta")
                        console.log(err.response.status)
                        reject()
                    })
            })
        },

    },
    mutations: {
        updateBook(state, book) {
            state.book = book
        },

        updateBooks(state, books) {
            state.books = books
        }

    },
    state: {

        book: {
            "@id": null,
            id: null,
            sku: null,
            name: null,
            price: null,
            weight: null,
            isDeleted: null
        },
        books: {
            models: [],
            totalItems: 0
        }

    },
    getters: {


        getBook(state) {
            return state.book
        },

        getBooks(state) {
            return state.books.models.filter((item) => {
                return item.isDeleted === false
            })
        },
        getBookIds(state) {
            let result = [];
            let obj = state.books.models.filter((item) => {
                return item.isDeleted === false
            });
            obj.forEach((item) => {
                result.push(item.sku);
            })
            return result;
        }




    }

}
import {createStore} from "vuex"
import book from "@/plugins/book"
import dvd from "@/plugins/dvd"
import furniture from "@/plugins/furniture"




export default createStore({
    modules: {
        book,
        dvd,
        furniture
    }
})